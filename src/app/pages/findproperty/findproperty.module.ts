import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FindpropertyRoutingModule } from './findproperty-routing.module';
import { FindpropertyComponent } from './findproperty.component';
import { FeatureCardComponent } from '../components/feature-card/feature-card.component';
import { ShareMdouleModule } from '../share-mdoule/share-mdoule.module';
import { FilterOptionComponent } from './component/filter-option/filter-option.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';

const icons = {
  funnel: allIcons.funnel
};

@NgModule({
  declarations: [
    FindpropertyComponent,
    FilterOptionComponent,

  ],
  imports: [
    CommonModule,
    ShareMdouleModule,
    FindpropertyRoutingModule,
    NgxBootstrapIconsModule.pick(icons),

  ]
})
export class FindpropertyModule { }
