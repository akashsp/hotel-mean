import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindpropertyComponent } from './findproperty.component';

const routes: Routes = [
  {
    path:'',
    component:FindpropertyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FindpropertyRoutingModule { }
