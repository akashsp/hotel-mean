import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindpropertyComponent } from './findproperty.component';

describe('FindpropertyComponent', () => {
  let component: FindpropertyComponent;
  let fixture: ComponentFixture<FindpropertyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FindpropertyComponent]
    });
    fixture = TestBed.createComponent(FindpropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
