import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BecameHostComponent } from './became-host.component';

import { ChoosePropertyComponent } from './components/choose-property/choose-property.component';
import { PlaceDescriptionComponent } from './components/place-description/place-description.component';

const routes: Routes = [
  {
    path: '',
    component: BecameHostComponent,
  },
  {
    path: 'add-property',
    component: ChoosePropertyComponent
  },
  {
    path: 'place-description',
    component: PlaceDescriptionComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BecameHostRoutingModule { }
