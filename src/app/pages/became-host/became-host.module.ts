import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BecameHostRoutingModule } from './became-host-routing.module';
import { BecameHostComponent } from './became-host.component';
import { HostComponent } from './components/host/host.component';
import { HostHomeComponent } from './components/host-home/host-home.component';
import { PlaceDescriptionComponent } from './components/place-description/place-description.component';
import { ChoosePropertyComponent } from './components/choose-property/choose-property.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';

const icons = {
  plusCircle: allIcons.plusCircle,
  dashCircle: allIcons.dashCircle
};

@NgModule({
  declarations: [
    BecameHostComponent,
    HostComponent,
    HostHomeComponent,
    PlaceDescriptionComponent,
    ChoosePropertyComponent

  ],
  imports: [
    CommonModule,
    BecameHostRoutingModule,
    NgxBootstrapIconsModule.pick(icons),
  ]
})
export class BecameHostModule { }
