import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosePropertyComponent } from './choose-property.component';

describe('ChoosePropertyComponent', () => {
  let component: ChoosePropertyComponent;
  let fixture: ComponentFixture<ChoosePropertyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChoosePropertyComponent]
    });
    fixture = TestBed.createComponent(ChoosePropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
