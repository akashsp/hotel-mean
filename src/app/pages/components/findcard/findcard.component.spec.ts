import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindcardComponent } from './findcard.component';

describe('FindcardComponent', () => {
  let component: FindcardComponent;
  let fixture: ComponentFixture<FindcardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FindcardComponent]
    });
    fixture = TestBed.createComponent(FindcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
