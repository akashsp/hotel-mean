import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailPageRoutingModule } from './detail-page-routing.module';
import { DetailPageComponent } from './detail-page.component';
import { ImagegalleryComponent } from './component/imagegallery/imagegallery.component';
import { BottomTabsComponent } from './component/bottom-tabs/bottom-tabs.component';
import { MainAmenitiesComponent } from './component/main-amenities/main-amenities.component';
import { TitlesComponent } from './component/titles/titles.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';
import { DescriptionComponent } from './component/description/description.component';
import { OfferedAmenitiesComponent } from './component/offered-amenities/offered-amenities.component';
import { SafetyHygieneComponent } from './component/safety-hygiene/safety-hygiene.component';
import { MapDetailComponent } from './component/map-detail/map-detail.component';
import { ReservationDetailComponent } from './component/reservation-detail/reservation-detail.component';
import { CommentComponent } from './component/Comments/comment/comment.component';
import { UserCommentsComponent } from './component/Comments/user-comments/user-comments.component';

const icons = {
  suitHeartFill: allIcons.suitHeartFill,
  suitHeart: allIcons.suitHeart,
  search: allIcons.search,
  shareFill: allIcons.shareFill,
  tv: allIcons.tv,
  wifi: allIcons.wifi,
  webcam: allIcons.webcam,
  starFill: allIcons.starFill,
  pinAngle:allIcons.pinAngle

};

@NgModule({
  declarations: [
    DetailPageComponent,
    ImagegalleryComponent,
    BottomTabsComponent,
    MainAmenitiesComponent,
    TitlesComponent,
    DescriptionComponent,
    OfferedAmenitiesComponent,
    SafetyHygieneComponent,
    MapDetailComponent,
    ReservationDetailComponent,
    CommentComponent,
    UserCommentsComponent,


  ],
  imports: [
    CommonModule,
    DetailPageRoutingModule,
    NgxBootstrapIconsModule.pick(icons)
  ]
})
export class DetailPageModule { }
