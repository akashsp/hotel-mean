import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferedAmenitiesComponent } from './offered-amenities.component';

describe('OfferedAmenitiesComponent', () => {
  let component: OfferedAmenitiesComponent;
  let fixture: ComponentFixture<OfferedAmenitiesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OfferedAmenitiesComponent]
    });
    fixture = TestBed.createComponent(OfferedAmenitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
