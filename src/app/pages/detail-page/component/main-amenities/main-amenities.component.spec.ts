import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAmenitiesComponent } from './main-amenities.component';

describe('MainAmenitiesComponent', () => {
  let component: MainAmenitiesComponent;
  let fixture: ComponentFixture<MainAmenitiesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MainAmenitiesComponent]
    });
    fixture = TestBed.createComponent(MainAmenitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
