import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureCardComponent } from '../components/feature-card/feature-card.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';
import { FindcardComponent } from '../components/findcard/findcard.component';

const icons = {
  suitHeartFill: allIcons.suitHeartFill,
  suitHeart: allIcons.suitHeart,
  search: allIcons.search,
  funnel:allIcons.funnel
};

@NgModule({
  declarations: [FeatureCardComponent, FindcardComponent],
  imports: [
    CommonModule,
    NgxBootstrapIconsModule.pick(icons),
  ],
  exports: [
    FeatureCardComponent,
    FindcardComponent
  ]
})
export class ShareMdouleModule { }
