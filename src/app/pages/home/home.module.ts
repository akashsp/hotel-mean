import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { BannerComponent } from './component/banner/banner.component';
import { HomeSearchComponent } from './component/home-search/home-search.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';
import { HomeCardComponent } from '../components/home-card/home-card.component';
import { AdBannerComponent } from './component/ad-banner/ad-banner.component';
// import { FeatureCardComponent } from '../components/feature-card/feature-card.component';
// import { FooterComponent } from '../footer/footer.component';
import { ShareMdouleModule } from '../share-mdoule/share-mdoule.module';


const icons = {
  suitHeartFill: allIcons.suitHeartFill,
  suitHeart: allIcons.suitHeart,
  search: allIcons.search,
  plusCircle:allIcons.plusCircle
};
@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
    HomeSearchComponent,
    HomeCardComponent,
    AdBannerComponent,
  
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgxBootstrapIconsModule.pick(icons),
    ShareMdouleModule
  ],



})
export class HomeModule { }
