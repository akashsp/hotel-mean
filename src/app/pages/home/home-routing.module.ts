import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'detail',
    loadChildren: () => import('../detail-page/detail-page.module').then(m => m.DetailPageModule)
  },
  {
    path: 'find-property',
    loadChildren: () => import('../findproperty/findproperty.module').then(m => m.FindpropertyModule)
  },
  {
    path: 'became-host',
    loadChildren: () => import('../became-host/became-host.module').then(m => m.BecameHostModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
